I know of two APIs for recreation.gov:
<https://ridb.recreation.gov/api/v1> and
<https://www.recreation.gov/api>.

# ridb.recreation.gov/api/v1

This is the [Recreation Information Database (RIDB)] API.  It is
documented at <https://ridb.recreation.gov/docs> and an [OpenAPI
specification] can be downloaded from
<https://ridb.recreation.gov/shared/swagger/ridb.yaml>.

[Recreation Information Database (RIDB)]: https://ridb.recreation.gov/
[OpenAPI specification]: https://github.com/OAI/OpenAPI-Specification

# www.recreation.gov/api

I haven't been able to find any documentation of this API.  It is used
by [recreation-gov-campsite-checker] and by
<https://www.recreation.gov/>.  `openapi.yaml` contains a partial
[OpenAPI specification] of this API.

[recreation-gov-campsite-checker]: https://github.com/banool/recreation-gov-campsite-checker
[OpenAPI specification]: https://github.com/OAI/OpenAPI-Specification
